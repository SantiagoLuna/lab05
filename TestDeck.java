import static org.junit.jupiter.api.Assertions.*;
import org.junit.*;
import org.junit.jupiter.api.Test;

public class TestDeck {
    
    @Test
    //check size not contents
    void testConstructor(){
        Deck deck = new Deck();
        assertEquals(112, deck.deck.size());
    }

    @Test
    void testAddToDeck(){
        Deck deck = new Deck();
        NumberedCards card1 = new NumberedCards("Red", 12);
       // NumberedCards card2 = new NumberedCards("Blue", 9);
        deck.addToDeck(card1);
       // deck.addToDeck(card2);

        assertEquals(card1, deck.deck.get(112));
    }

    @Test
    void testDraw(){
        Deck deck = new Deck();
        NumberedCards card1 = new NumberedCards("Red", 12);
        deck.deck.set(0, card1);
        assertEquals(card1, deck.draw());
    }

    @Test
    void testShuffle(){
        Deck deck = new Deck();
        deck.shuffle();
        assertEquals(deck.deck.get(0), deck.draw());
    }

}
