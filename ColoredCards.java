public abstract class ColoredCards implements Cards {
    protected String color;

    public ColoredCards(String inColor){
        this.color = inColor;
    }

    public String getColor(){
        return this.color;
    }
    
    @Override
    public boolean canPlay(Cards card){
        if(card instanceof ColoredCards){
            if(this.color.equals(card.getColor())){
                return true;
            }
            return false;
        }
        return false;
    }
}
