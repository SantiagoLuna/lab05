public class PickUp2Card extends SpecialCards implements SkipEffect {
    
    public PickUp2Card(String color){
        super(color);
    }
    @Override
    public boolean canPlay(Cards card){
        if(card instanceof PickUp2Card){
            return true;
        }
        return false;
    }

    @Override
    public void skip(){
        System.out.println("You played a pick up 2 card, next person losses their turn!");
    }

    public void pickUp(){
        System.out.println("Next player picks up 2 cards!");
    }
}
