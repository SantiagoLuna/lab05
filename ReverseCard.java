public class ReverseCard extends SpecialCards {
    public ReverseCard(String incolor){
        super(incolor);
    }
    
    @Override
    public boolean canPlay(Cards card){
        if(card instanceof ReverseCard){
            return true;
        }
        return false;
    }
}
