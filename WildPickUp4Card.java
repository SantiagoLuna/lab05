public class WildPickUp4Card extends Wilds implements SkipEffect {
    
    public WildPickUp4Card(){
        super();
    }

    @Override
    public void skip(){
        System.out.println("You played wildPickUp4Card, skip next player's turn.");
    }
    
    public void pickUp(){
        System.out.println("Next player picks up 4 cards!");
    }
}
