/**
 * @author Santiago Luna 2032367
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Deck {
    //change from public
    public ArrayList<Cards> deck = new ArrayList<Cards>();

    public Deck(){
        //adding WildCards
        deck.addAll(Arrays.asList(generateWildCards()));
        //adding WildPickUp4Cards
        deck.addAll(Arrays.asList(generatePickUp4Cards()));
        //adding NumberedCards
        deck.addAll(Arrays.asList(generateNumberedCards()));
        //adding SpecialCards
        deck.addAll(Arrays.asList(generateSkipCards()));
        deck.addAll(Arrays.asList(generatePickUp2Cards()));
        deck.addAll(Arrays.asList(generateReverseCards()));

    }

    public void addToDeck(Cards card){
        deck.add(card);
    }

    public Cards draw(){
        //shouldnt be able to instantiate card obj
        Cards card = deck.get(0);
        deck.remove(0);
        return card;
    }

    public void shuffle(){
        ArrayList<Cards> shuffledDeck = new ArrayList<Cards>();
        Random rand = new Random();
        
        for(int i=0; i<deck.size(); i++){
            int randNum = rand.nextInt(deck.size());
            shuffledDeck.add(deck.get(randNum));
        }
        deck = shuffledDeck;
    }

    public static WildCard[] generateWildCards(){
        WildCard[] wildCards = new WildCard[4];
        for(int i=0; i< wildCards.length; i++){
            wildCards[i] = new WildCard();
        }
        return wildCards;
    }

    public static WildPickUp4Card[] generatePickUp4Cards(){
        WildPickUp4Card[] wild4Cards = new WildPickUp4Card[4];
        for(int i=0; i< wild4Cards.length; i++){
            wild4Cards[i] = new WildPickUp4Card();
        }
        return wild4Cards;
    }

    public static NumberedCards[] generateNumberedCards(){
        NumberedCards[] cards = new NumberedCards[80];
        for(int i=0; i<cards.length; i++){
            if(i<=20){
                cards[i] = new NumberedCards("Green", i%10);
            }
            else if(i<=40){
                cards[i] = new NumberedCards("Blue", i%10);
            }
            else if(i<=60){
                cards[i] = new NumberedCards("Red", i%10);
            }
            else if(i<=80){
                cards[i] = new NumberedCards("Yellow", i%10);
            }
        }
        return cards;
    }

    // public static SpecialCards[] generateSpecialCards(){
    //     SpecialCards[] cards = new SkipCard[24];

    //     for(int i=0; i<cards.length; i++){
    //         if(i<=2){
    //             cards[i] = new SkipCard("Green");  
    //         }
    //         if(i<=4){
    //             cards[i] = new SkipCard("Blue");
    //         }
    //         if(i<=6){
    //             cards[i] = new SkipCard("Red");
    //         }
    //         if(i<=8){
    //             cards[i] = new SkipCard("Yellow");
    //         }
    //         if(i<=10){
    //             cards[i] = new ReverseCard("Green");
    //         }
    //         if(i<=12){
    //             cards[i] = new ReverseCard("Blue");
    //         }
    //         if(i<=14){
    //             cards[i] = new ReverseCard("Red");
    //         }
    //         if(i<=16){
    //             cards[i] = new ReverseCard("Yellow");
    //         }
    //         if(i<=18){
    //             cards[i] = new PickUp2Card("Green");
    //         }
    //         if(i<=20){
    //             cards[i] = new PickUp2Card("Blue");
    //         }
    //         if(i<=22){
    //             cards[i] = new PickUp2Card("Red");
    //         }
    //         if(i<=24){
    //             cards[i] = new PickUp2Card("Yellow");
    //         }
    //     }
    //     return cards;
    // } 

    public static SkipCard[] generateSkipCards(){
        SkipCard[] cards = new SkipCard[8];
        for(int i=0; i<cards.length; i++){
            if(i<=2){
                cards[i] = new SkipCard("Green");  
            }
            if(i<=4){
                cards[i] = new SkipCard("Blue");
            }
            if(i<=6){
                cards[i] = new SkipCard("Red");
            }
            if(i<=8){
                cards[i] = new SkipCard("Yellow");
            }
        }
        return cards;
    }
    
    public static PickUp2Card[] generatePickUp2Cards(){
        PickUp2Card[] cards = new PickUp2Card[8];
        for(int i=0; i<cards.length; i++){
            if(i<=2){
                cards[i] = new PickUp2Card("Green");  
            }
            if(i<=4){
                cards[i] = new PickUp2Card("Blue");
            }
            if(i<=6){
                cards[i] = new PickUp2Card("Red");
            }
            if(i<=8){
                cards[i] = new PickUp2Card("Yellow");
            }
        }
        return cards;
    }

    public static ReverseCard[] generateReverseCards(){
        ReverseCard[] cards = new ReverseCard[8];
        for(int i=0; i<cards.length; i++){
            if(i<=2){
                cards[i] = new ReverseCard("Green");  
            }
            if(i<=4){
                cards[i] = new ReverseCard("Blue");
            }
            if(i<=6){
                cards[i] = new ReverseCard("Red");
            }
            if(i<=8){
                cards[i] = new ReverseCard("Yellow");
            }
        }
        return cards;
    }

}

