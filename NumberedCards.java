public class NumberedCards extends ColoredCards{
    private int number;

    public NumberedCards(String color, int num){
        super(color);
        this.number = num;
    }

    public int getNumber(){
        return this.number;
    }

    @Override
    public boolean canPlay(Cards card){
        //first check instance of
        if(card instanceof NumberedCards){
            if(this.number == ((NumberedCards) card).getNumber()){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Cards){
         NumberedCards card = (NumberedCards) o;
         if(this.number == card.getNumber() && this.color.equals(card.getColor())){
             return true;
         }   
        }
        return false;
    }
    
}