public class SkipCard extends SpecialCards implements SkipEffect{
    
    public SkipCard(String color){
        super(color);
    }
    @Override
    public void skip(){
        System.out.println("Drew skip card, skipped your turn");
    }

    @Override
    public boolean canPlay(Cards card){
        if(card instanceof SkipCard){
            return true;
        }
        return false;
    }
}
